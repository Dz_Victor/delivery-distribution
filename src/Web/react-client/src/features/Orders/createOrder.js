import { Container, Form, FormGroup, Jumbotron, Button, Col} from "react-bootstrap"
import React, { useState } from 'react';
import { useHistory } from "react-router-dom";

import { useDispatch } from 'react-redux'
import { unwrapResult } from '@reduxjs/toolkit'

import {addOrder} from "./orderSlice";
import OrderService from "./orderService";

const CreateOrder = () =>{

    const initialState = {
        destinationAddress: "",
        departureAddress: "",
        weight: 0,
        price: 0
    };

    const [state, setState] = useState(initialState);

    const [addRequestStatus, setAddRequestStatus] = useState('idle');

    const [getPriceRequestStatus, setGetPriceRequestStatus] = useState('idle');

    const dispatch = useDispatch();

    let history = useHistory()

    const canSave =
        [state.destinationAddress, state.departureAddress, state.weight].every(Boolean)
        && state.price > 0 
        && addRequestStatus === 'idle';

    const canGetPrice = 
        [state.destinationAddress, state.departureAddress, state.weight].every(Boolean)
        && getPriceRequestStatus === 'idle';

    const handleChange = (e) => {
        const {name, value} = e.target;
        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const getPrice = async() =>{
        if(canGetPrice){
            try{
                setGetPriceRequestStatus('pending');
                const response = await OrderService.getPrice({
                    departureAddress: state.departureAddress,
                    destinationAddress: state.destinationAddress,
                    weight: state.weight
                })
                
                setState(prevState => ({
                                ...prevState,
                                'price': response.data
                            }));
            }
            catch(err){
                console.error('Failed to get price: ', err);
            }
            finally{
                setGetPriceRequestStatus('idle');
            }
        }
        
    }

    const submitForm = async () =>{
        if(canSave){
            let isSuccess = false;
            try{
                setAddRequestStatus('pending')
                const resultAction = await dispatch(
                    addOrder(state));
                unwrapResult(resultAction);
                setState(initialState);
                isSuccess = true;
            }
            catch (err) {
                console.error('Failed to save order: ', err)
              } 
            finally {
                setAddRequestStatus('idle')
                if(isSuccess){
                    history.push("/orderList");
                }
              }
        }
    }

    return(
        <Container className="vertical-center">
            <Jumbotron >
                <h1>Рассчитать доставку</h1>
                <hr/>
                <Form> 
                    <FormGroup>
                        <Form.Label 
                            className="font-weight-bold"
                            >
                            Откуда
                        </Form.Label>
                        <Form.Control 
                                type="text"
                                name="departureAddress"
                                id="departureAddress"
                                placeholder="Адрес отправления"
                                value={state.departureAddress}
                                onChange={handleChange}
                                required    
                            />
                    </FormGroup>

                    <FormGroup>
                        <Form.Label 
                            className="font-weight-bold"
                            >
                            Куда
                        </Form.Label>
                        <Form.Control 
                                type="text"
                                name="destinationAddress"
                                id="destinationAddress"
                                placeholder="Адрес получения"
                                value={state.destinationAddress}
                                onChange={handleChange}
                                required    
                            />
                    </FormGroup>

                    <FormGroup>
                        <Form.Label 
                            className="font-weight-bold"
                            >
                            Вес посылки, кг
                        </Form.Label>
                        <Form.Control 
                                type="number"
                                min="0"
                                name="weight"
                                id="weight"
                                placeholder="Вес посылки"
                                value={state.weight}
                                onChange={handleChange}
                                required    
                            />
                    </FormGroup>


                    <FormGroup>
                        <Form.Label 
                            className="font-weight-bold"
                            >
                            Стоимость доставки, руб
                        </Form.Label>
                        <Form.Row>
                            <Col sm={4}>
                                <Form.Control
                                    readOnly
                                    type="text"
                                    onChange={handleChange}
                                    value={state.price}
                                    />
                            </Col>
                            <Col>
                                <Button variant="info"
                                        onClick={getPrice}
                                        disabled={!canGetPrice}>
                                    Рассчитать стоимость
                                </Button>
                            </Col>
                        </Form.Row>
                    </FormGroup>

                
                    <FormGroup >
                        <Button 
                            variant="primary"  
                            onClick={() => submitForm()}
                            size="lg" 
                            block
                            disabled={!canSave}>
                            Создать
                        </Button>
                    </FormGroup>
                </Form>
            </Jumbotron>
        </Container>
    );
};

export default CreateOrder;