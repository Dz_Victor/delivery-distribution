import {createClient} from "../../api/http-client";

//Создать заказ
const create = data =>{
    return createClient().post("/orders", data);
}

//Получить стоимость заказа
const getPrice = data =>{
    return createClient().get("/orders/Compute-price", data);
}

//Получить все заказы для выбранного покупателя
const getByCustomerId = data =>{
    return createClient().get("/orders", {params:data});
}

//Получить заказ по id
const getById = id =>{
    return createClient().get(`/orders/${id}`);
}

//Отметить заказ оплаченным
const setPaidById = id =>{
    return createClient().post(`orders/${id}/setPaid`);
}

//Отменить заказ по id
const cancelOrderById = (id) =>{
    return createClient().post(`orders/${id}/setStatus`, {state:6});
}

export default {
    create,
    getPrice,
    getByCustomerId,
    getById,
    setPaidById,
    cancelOrderById
};