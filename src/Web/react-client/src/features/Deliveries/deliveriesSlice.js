import {
    createAsyncThunk,
    createSlice,
    createEntityAdapter
} from "@reduxjs/toolkit";

import DeliveryService from "./deliveryService";

const deliveriesAdapter = createEntityAdapter({
    sortComparer: (a, b) => b.createdOn.localeCompare(a.createdOn),
});

const initialState = deliveriesAdapter.getInitialState({
    status: "idle",
    error: null
});

export const fetchDeliveriesByOrder = createAsyncThunk(
    'deliveries/getByorder',
    async(orderId) =>{
        const response = await DeliveryService.getByOrderId(orderId);
        return response.data;
    }
);

const deliveriesSlice = createSlice({
    name: "deliveries",
    initialState,
    reducers:{
        deliveriesUpdated:(state, action) =>{
            state.status = "idle";
        }
    },
    extraReducers:{
        [fetchDeliveriesByOrder.pending]:(state, action) =>{
            state.status = "loading";
        },
        [fetchDeliveriesByOrder.fulfilled]:(state, action) =>{
            state.status = "succeeded";
            deliveriesAdapter.setAll(state, action.payload);
        },
        [fetchDeliveriesByOrder.rejected]: (state, action) =>{
            state.status = "failed";
            state.error = action.error.message;
        }
    }
});

export default deliveriesSlice.reducer;

export const {deliveriesUpdated} = deliveriesSlice.actions;

export const {
    selectAll: selectAllDeliveries,
    selectById: selectDeliveryById,
    selectIds: selectDeliveriesIds
  } = deliveriesAdapter.getSelectors((state) => state.deliveries);