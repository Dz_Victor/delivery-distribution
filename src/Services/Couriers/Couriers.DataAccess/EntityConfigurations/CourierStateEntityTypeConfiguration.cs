﻿using Couriers.Core.Domain.Enumerations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Couriers.DataAccess.EntityConfigurations
{
    public class CourierStateEntityTypeConfiguration: 
        IEntityTypeConfiguration<CourierState>
    {
        public void Configure(EntityTypeBuilder<CourierState> builder)
        {
            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id)
                .HasDefaultValue(1)
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(o => o.Name)
                .HasMaxLength(200)
                .IsRequired();
        }
    }
}