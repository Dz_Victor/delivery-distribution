﻿using Couriers.WebHost.Dto.Courier;
using FluentValidation;

namespace Couriers.WebHost.Validation.Courier
{
    public class CreateOrEditCourierRequestValidator: AbstractValidator<CreateOrEditCourierRequest>
    {
        public CreateOrEditCourierRequestValidator()
        {
            this.RuleFor(request => request.Name).NotEmpty().Length(5, 30);
            //проверяем, что количество символов совпадает со стандартной длиной номера +7... или 8...
            this.RuleFor(request => request.PhoneNumber).NotEmpty().Length(11, 12);
        }
    }
}