using System;

namespace Couriers.WebHost.Dto.Courier
{
    public record CreateOrEditCourierRequest
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public Guid VehicleId { get; set; }
    }
}