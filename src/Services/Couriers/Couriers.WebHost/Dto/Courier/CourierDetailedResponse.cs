﻿using System;
using System.Collections.Generic;

namespace Couriers.WebHost.Dto.Courier
{
    public record CourierDetailedResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public Guid VehicleId { get; set; }
        public string VehicleName { get; set; }
        public int VehicleLoadCapacity { get; set; }
        public string State { get; set; }
    }
}
