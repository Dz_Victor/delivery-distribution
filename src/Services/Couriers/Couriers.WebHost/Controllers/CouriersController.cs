using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System;
using AutoMapper;
using Couriers.Core.Domain;
using Couriers.WebHost.Dto.Courier;
using Couriers.Core.Abstraction.Services;

namespace Couriers.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CouriersController : ControllerBase
    {
        private readonly ICourierService _courierService;
        private readonly IMapper _mapper;

        public CouriersController(ICourierService courierService, 
                                IMapper mapper)
        {
            _courierService = courierService;
            _mapper = mapper;            
        }

        /// <summary>
        /// Получить список курьеров
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CourierShortResponse>>> GetCouriersAsync()
        {
            var couriers = await _courierService.GetCouriersAsync();
            var response = _mapper.Map<IEnumerable<CourierShortResponse>>(couriers);
            return Ok(response);
        }

        /// <summary>
        /// Получить данные курьера по идентификатору
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}", Name = "GetCourierAsync")]
        public async Task<ActionResult<CourierDetailedResponse>> GetCourierAsync(Guid id)
        {
            var courier = await _courierService.GetCourierByIdAsync(id);
            var response = _mapper.Map<CourierDetailedResponse>(courier);
            return Ok(response);
        }
        
        /// <summary>
        /// Создать нового курьера
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddCourierAsync(CreateOrEditCourierRequest request)
        {
            var newCourier =  _mapper.Map<Courier>(request);
            var responseId = await _courierService.AddCourierAsync(newCourier);
            return CreatedAtRoute(nameof(GetCourierAsync), new { id = responseId }, responseId);
        }
        
        /// <summary>
        /// Изменить данные курьера по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCouriersAsync(Guid id, CreateOrEditCourierRequest request)
        {
            var updatedCourier = new Courier();
            _mapper.Map(request, updatedCourier);
            await _courierService.EditCourierAsync(id, updatedCourier);
            return Ok();
        }

        /// <summary>
        /// Удалить курьера по идентификатору вместе с выданными ему доставками 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCourierAsync(Guid id)
        {
            await _courierService.DeleteCourierAsync(id);
            return Ok();
        }
        /// <summary>
        /// Изменить статус курьера по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>        
        [HttpPut("{id:guid}/Status")]
        public async Task<IActionResult> EditCourierStatusAsync(Guid id, EditCourierStatusRequest request)
        {
            var newStateId = request.State;
            await _courierService.EditCourierStatusAsync(id, newStateId);
            return Ok();
        }

        /// <summary>
        /// Изменить транспорт курьера
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}/Vehicle")]
        public async Task<IActionResult> EditCouriersVehicleByIdAsync(Guid id, ChangeCouriersVehicleRequest request)
        {
            await _courierService.UpdateCourierVehicleByIdAsync(id, request.VehicleId);
            return Ok();
        }
        
    }
}