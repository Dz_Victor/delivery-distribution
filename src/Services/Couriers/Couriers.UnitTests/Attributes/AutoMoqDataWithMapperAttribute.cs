using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using AutoMapper;
using Couriers.WebHost.Mappers;
using AutoMapperCourierProfiles = Couriers.WebHost.Mappers.AutoMapperCourierProfiles;

namespace Couriers.UnitTests.Attributes
{
    public class AutoMoqDataWithMapperAttribute: AutoDataAttribute
    {
        public AutoMoqDataWithMapperAttribute()
            : base(() =>
            {
                var fixture = new Fixture().Customize(new AutoMoqCustomization());
                fixture.Register(() =>
                {
                    IMapper mapper = new Mapper(new MapperConfiguration(cfg =>
                    {
                        cfg.AddProfile<AutoMapperCourierProfiles>();
                    }));
                    return mapper;
                });
                return fixture;
            })
        {
        }
    }
}