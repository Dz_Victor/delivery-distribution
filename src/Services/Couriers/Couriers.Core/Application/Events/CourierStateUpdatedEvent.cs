﻿using System;
using MediatR;

namespace Couriers.Core.Application.Events
{
    public class CourierStateUpdatedEvent:INotification
    {
        public Guid CourierId { get; set; }
        
        public int CourierStateId { get; set; }
    }
}