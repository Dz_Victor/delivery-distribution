using System;
using MediatR;

namespace Couriers.Core.Application.Commands
{
    public class CheckCouriersDeliveriesCommand:IRequest<bool>
    {
        public Guid CourierId { get; set; }
    }
}