using System;
using Couriers.Core.Abstraction;

namespace Couriers.Core.Application
{
    public class CurrentDateTimeProvider:ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}