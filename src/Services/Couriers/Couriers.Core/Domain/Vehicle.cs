﻿using System.Collections.Generic;
using Couriers.Core.Domain.Enumerations;


namespace Couriers.Core.Domain
{
    public class Vehicle : BaseEntity
    {
        /// <summary>
        /// Название транспортного средства
        /// </summary>
        public string? Name { get; set; }

        
        /// <summary>
        /// Грузоподъемность (кг)
        /// </summary>
        public int LoadCapacity { get; set; }

        /// <summary>
        /// Навигационное свойство для связи с сущностью Курьер
        /// </summary>

        public virtual ICollection<Courier>? Couriers { get; set; }

    }
}
