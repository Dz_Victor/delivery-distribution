﻿using System;

namespace Deliveries.WebHost.Dto.Delivery
{
    public class CreateDeliveryRequest 
    {
        /// <summary>
        /// Id курьера, выполняющего доставку
        /// </summary>
        public Guid? CourierId { get; set; }
        
        /// <summary>
        /// Id заказа, для которого выполняется доставка
        /// </summary>
        public Guid OrderId { get; set; }
    }
}