using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Application.Services;
using Deliveries.Core.Domain;
using Deliveries.UnitTests.Attributes;
using Deliveries.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Deliveries.UnitTests.Core.Application.Services.DeliveryServiceTests
{
    public class AddDeliveryAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task AddDeliveryAsync_CreatedOnIsNull_CreatedOnIsNow(
            [Frozen] Mock<ICurrentDateTimeProvider> mockDateTimeProvider,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var delivery = DeliveryBuilder
                .CreateBase()
                .WithCreatedOn(new DateTime())
                .WithUpdatedOn(new DateTime());
            mockDateTimeProvider.Setup(provider => provider.CurrentDateTime)
                .Returns(new DateTime(2021, 02, 01));

            //act
            await deliveryService.AddDeliveryAsync(delivery);
            
            //assert
            delivery.CreatedOn.Should().Be(mockDateTimeProvider.Object.CurrentDateTime);
        }

        [Theory, AutoMoqData]
        public async Task AddDeliveryAsync_UpdatedIsNull_UpdatedIsNow(
            [Frozen] Mock<ICurrentDateTimeProvider> mockDateTimeProvider,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var delivery = DeliveryBuilder
                .CreateBase()
                .WithUpdatedOn(new DateTime());
            mockDateTimeProvider.Setup(provider => provider.CurrentDateTime)
                .Returns(new DateTime(2021, 02, 01));
            
            //act
            await deliveryService.AddDeliveryAsync(delivery);
            
            //assert
            delivery.UpdatedOn.Should().Be(mockDateTimeProvider.Object.CurrentDateTime);
        }

        [Theory, AutoMoqData]
        public async Task AddDeliveryAsync_BaseRequest_AddIsInvoked(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<ICurrentDateTimeProvider> mockDateTime,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var now = new DateTime(2021, 02, 01);
            var delivery = DeliveryBuilder
                .CreateBase()
                .WithCreatedOn(now)
                .WithUpdatedOn(now);

            mockDateTime.Setup(dt => dt.CurrentDateTime)
                .Returns(now);

            mockRepository.Setup(repo => repo.AddAsync(delivery))
                .ReturnsAsync(delivery.Id);
            
            //act
            await deliveryService.AddDeliveryAsync(delivery);
            
            //assert
            mockRepository.Verify(repo => repo.AddAsync(delivery), Times.Once);
        }
    }
}