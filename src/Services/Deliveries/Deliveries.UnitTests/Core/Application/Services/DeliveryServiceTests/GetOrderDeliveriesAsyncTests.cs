using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Application.Services;
using Deliveries.Core.Domain;
using Deliveries.UnitTests.Attributes;
using Deliveries.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Deliveries.UnitTests.Core.Application.Services.DeliveryServiceTests
{
    public class GetOrderDeliveriesAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetOrderDeliveriesAsync_DeliveryExists_ReturnResponse(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase();
            var deliveryList = new List<Delivery>()
            {
                delivery
            };
            mockRepository.Setup(repo => repo.GetOnConditionAsync(It.IsAny<Expression<Func<Delivery, bool>>>()))
                .ReturnsAsync(deliveryList);
            
            //act
            var result = await deliveryService.GetOrderDeliveriesAsync(delivery.OrderId);
            
            //assert
            result.Should().BeEquivalentTo(deliveryList);
        }
    }
}