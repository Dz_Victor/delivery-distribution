using Deliveries.Core.Domain;
using Deliveries.Core.Enumerations;
using Deliveries.DataAccess.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Deliveries.DataAccess.Data
{
    public class DataContext: DbContext
    {
        public DbSet<Delivery> Deliveries { get; set; }
        
        public DbSet<DeliveryState> DeliveryStates { get; set; }
        
        public DbSet<DeliveryStateHistory> DeliveryStateHistories { get; set; }
        
        
        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DeliveryStateEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DeliveryEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}