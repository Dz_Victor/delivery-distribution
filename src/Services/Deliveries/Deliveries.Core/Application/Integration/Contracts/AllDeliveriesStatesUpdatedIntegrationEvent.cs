﻿// ReSharper disable CheckNamespace

using System;

namespace DeliveryDistribution.Integration.Contracts
{
    // ReSharper disable once InconsistentNaming
    public interface AllDeliveriesStatesUpdatedIntegrationEvent
    {
        public Guid OrderId { get; set; }
        
        public int DeliveryStateId { get; set; }
    }
}