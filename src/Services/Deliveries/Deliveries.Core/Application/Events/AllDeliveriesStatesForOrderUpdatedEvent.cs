﻿using System;
using MediatR;

namespace Deliveries.Core.Application.Events
{
    public class AllDeliveriesStatesForOrderUpdatedEvent: INotification
    {
        public Guid OrderId { get; set; }
        
        public int DeliveryStateId { get; set; }
        
    }
}