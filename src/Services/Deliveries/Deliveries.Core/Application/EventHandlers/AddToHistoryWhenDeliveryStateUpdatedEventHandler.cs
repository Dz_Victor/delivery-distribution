﻿using System.Threading;
using System.Threading.Tasks;
using Deliveries.Core.Abstraction.Services;
using Deliveries.Core.Application.Events;
using Deliveries.Core.Domain;
using MediatR;

namespace Deliveries.Core.Application.EventHandlers
{
    public class AddToHistoryWhenDeliveryStateUpdatedEventHandler
        :INotificationHandler<DeliveryStateUpdatedEvent>
    {

        private readonly IDeliveryStateHistoryService _deliveryStateHistoryService;

        public AddToHistoryWhenDeliveryStateUpdatedEventHandler(
            IDeliveryStateHistoryService deliveryStateHistoryService)
        {
            _deliveryStateHistoryService = deliveryStateHistoryService;
        }

        public async Task Handle(DeliveryStateUpdatedEvent notification, CancellationToken cancellationToken)
        {
            var deliveryStateHistory = new DeliveryStateHistory()
            {
                DeliveryId = notification.DeliveryId,
                DeliveryStateId = notification.DeliveryStateId
            };
            await _deliveryStateHistoryService.AddDeliveryStateAsync(deliveryStateHistory);
        }
    }
}