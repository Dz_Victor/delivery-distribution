﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Domain;
using Orders.DataAccess.Data;

namespace Orders.DataAccess.Repositories
{
    public class OrdersRepository:IRepository<Order>
    {
        private readonly DataContext _dataContext;

        public OrdersRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<Order>> GetAllAsync()
        {
            var entities = await _dataContext
                .Orders
                .Include(o => o.OrderState)
                .AsNoTrackingWithIdentityResolution()
                .ToListAsync();
            return entities;
        }

        public async Task<Order?> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext
                .Orders
                .Include(d => d.OrderState)
                .FirstOrDefaultAsync(x => x.Id == id);
            return entity;
        }

        public async Task<Guid> AddAsync(Order entity)
        {
            _dataContext.Orders.Add(entity);
            await _dataContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(Order entity)
        {
            _dataContext.Orders.Update(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Order entity)
        {
            _dataContext.Orders.Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Order>> GetOnConditionAsync(Expression<Func<Order, bool>> predicate)
        {
            return await _dataContext.Orders
                .Include(d => d.OrderState)
                .AsNoTrackingWithIdentityResolution()
                .Where(predicate)
                .ToListAsync();
        }
    }
}