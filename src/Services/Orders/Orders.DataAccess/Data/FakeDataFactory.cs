﻿using System;
using System.Collections.Generic;
using Orders.Core.Domain;
using Orders.Core.Enumerations;

namespace Orders.DataAccess.Data
{
    public static class FakeDataFactory
    {

        public static IEnumerable<Order> Orders => new List<Order>()
        {
            new Order()
            {
                Id = Guid.Parse("2f36025d-50ac-4b14-a9af-b4a838e580d2"),
                Code = "A00001",
                CreatedOn = new DateTime(2021, 04, 01),
                DepartureAddress = "SomeDepartureAddress",
                DestinationAddress = "SomeDestinationAddress",
                Price = 10,
                OrderStateId = OrderState.New.Id,
                UpdatedOn = new DateTime(2021, 04, 01),
                Weight = 10,
                CustomerId = Guid.Parse("5540c4e7-0d75-448c-bad4-68d00700fe98"),
                IsPaid = false
            },
            new Order()
            {
                Id = Guid.Parse("ab51377c-c478-4861-9e3a-5de58e5cf2e1"),
                Code = "A00002",
                CreatedOn = new DateTime(2021, 04, 02),
                DepartureAddress = "SomeOtherDepartureAddress",
                DestinationAddress = "SomeOtherDestinationAddress",
                Price = 15,
                OrderStateId = OrderState.New.Id,
                UpdatedOn = new DateTime(2021, 04, 02),
                Weight = 24,
                CustomerId = Guid.Parse("092d7ae5-8c71-448c-bfc8-13f2f3f9a80b"),
                IsPaid = true
            },
        };

        public static IEnumerable<OrderStateHistory> OrderStateHistories => new List<OrderStateHistory>()
        {
            new OrderStateHistory()
            {
                Id = Guid.Parse("2918434b-5a80-4cbd-85c6-0e06d5ee5b9c"),
                OrderId = Guid.Parse("2f36025d-50ac-4b14-a9af-b4a838e580d2"),
                OrderStateId = OrderState.New.Id,
                UpdatedOn = new DateTime(2021, 03, 31)
            },
            new OrderStateHistory()
            {
                Id = Guid.Parse("7f6cf0f1-ceb6-4d0b-b16a-ed4636c7f319"),
                OrderId = Guid.Parse("ab51377c-c478-4861-9e3a-5de58e5cf2e1"),
                OrderStateId = OrderState.New.Id,
                UpdatedOn = new DateTime(2021, 03, 30)
            }
        };
    }
}
