﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Application.Services;
using Orders.Core.Domain;
using Orders.UnitTests.Attributes;
using Orders.UnitTests.Builders.Domain;
using Xunit;

namespace Orders.UnitTests.Core.Application.Services.OrderServiceTests
{
    public class GetOrdersAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetOrdersAsync_OrdersNotFound_ReturnsEmptyList(
             [Frozen] Mock<IRepository<Order>> mockRepository,
             [Frozen] OrderService orderService
            )
        {
            //arrange
            mockRepository.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(new List<Order>());

            //act
            var result = await orderService.GetOrdersAsync();

            //assert
            result.Should().BeEmpty();
        }

        [Theory, AutoMoqData]        
        public async Task GetOrdersAsync_OrderExists_ReturnOrders(
            [Frozen] Mock<IRepository<Order>> mockRepository,
            [Frozen] OrderService orderService
            )
        {
            //arrange
            var order = OrderBuilder.CreateBase();
            var orderList = new List<Order>()
            {
                order
            };
            mockRepository.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(orderList);

            //act
            var result = await orderService.GetOrdersAsync();

            //arrange
            result.Should().BeEquivalentTo(orderList);
        }
    }
}