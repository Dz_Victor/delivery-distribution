﻿using System;

namespace Orders.WebHost.Dto.Order
{
    public record CreateOrEditOrderRequest
    {
        public string DepartureAddress { get; set; }
        public string DestinationAddress { get; set; }
        public double Weight { get; set; }
        public double Price { get; set; }        
        public Guid CustomerId { get; set; }
    }
}