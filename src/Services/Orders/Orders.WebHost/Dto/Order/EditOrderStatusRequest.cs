﻿namespace Orders.WebHost.Dto.Order
{
    public record EditOrderStatusRequest
    {
        public int State { get; set; }
    }
}