﻿using System;

namespace Orders.WebHost.Dto.Order
{
    public record OrderStateHistoryResponse
    {
        public StateResponse OrderState { get; set; }
        
        public DateTime UpdatedOn { get; set; }
    }
}