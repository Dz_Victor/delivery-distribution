﻿using AutoMapper;
using Orders.Core.Domain;
using Orders.WebHost.Dto.Order;

namespace Orders.WebHost.Mappers
{
    public class AutoMapperOrderProfiles: Profile
    {
        public AutoMapperOrderProfiles()
        {
            CreateMap<Order, OrderDetailedResponse>().ReverseMap();

            CreateMap<CreateOrEditOrderRequest, Order>();

            CreateMap<Order, EditOrderStatusRequest>();
            CreateMap<EditOrderStatusRequest, Order>()
                .ForMember(dst => dst.OrderStateId,
                    opt => opt.MapFrom(
                        src => src.State));

            CreateMap<Order, OrderShortResponse>().ReverseMap();

            CreateMap<OrderStateHistory, OrderStateHistoryResponse>();
        }
    }
}