﻿using AutoMapper;
using Orders.Core.Enumerations;
using Orders.WebHost.Dto;

namespace Orders.WebHost.Mappers
{
    public class AutoMapperStateProfiles: Profile
    {
        public AutoMapperStateProfiles()
        {
            CreateMap<OrderState, StateResponse>()
                .ForMember(dst => dst.State, 
                    opt => opt.MapFrom(src =>
                        src.Name));
        }
    }
}