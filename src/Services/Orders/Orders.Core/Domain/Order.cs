using System;
using Orders.Core.Enumerations;

namespace Orders.Core.Domain
{
    
    public class Order: BaseEntity
    {
        /// <summary>
        /// Адрес отправления
        /// </summary>
        public string? DepartureAddress { get; set; }
        
        /// <summary>
        /// Адрес назначения
        /// </summary>
        public string? DestinationAddress { get; set; }
        
        /// <summary>
        /// Вес заказа
        /// </summary>
        public double Weight { get; set; }
        
        /// <summary>
        /// Стоимость заказа
        /// </summary>
        public double Price { get; set; }
        
        /// <summary>
        /// Номер/Код заказа
        /// </summary>
        public string? Code { get; set; }
        
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedOn { get; set; }
        
        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime UpdatedOn { get; set; }
        
        /// <summary>
        /// Id текущего состояния заказа
        /// </summary>
        public int OrderStateId { get; set; }
        
        /// <summary>
        /// Текущее состояние заказа
        /// </summary>
        public OrderState OrderState { get; set; }
        
        /// <summary>
        /// Id клиента
        /// </summary>
        public Guid CustomerId { get; set; }
        
        /// <summary>
        /// Метка оплаты заказа
        /// </summary>
        public bool IsPaid { get; set; }

    }
}