﻿using System;
using Orders.Core.Enumerations;

namespace Orders.Core.Domain
{
    public class OrderStateHistory: BaseEntity
    {
        /// <summary>
        /// Id заказа
        /// </summary>
        public Guid OrderId { get; set; }
        
        /// <summary>
        /// Заказ
        /// </summary>
        public Order Order { get; set; }
        
        /// <summary>
        /// Id статуса заказа
        /// </summary>
        public int OrderStateId { get; set; }
        
        /// <summary>
        /// Статус заказа
        /// </summary>
        public OrderState OrderState { get; set; }
        
        /// <summary>
        /// Время обновления состояния заказа
        /// </summary>
        public DateTime UpdatedOn { get; set; }
    }
}