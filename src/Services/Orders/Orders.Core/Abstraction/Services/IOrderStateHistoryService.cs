﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Orders.Core.Domain;

namespace Orders.Core.Abstraction.Services
{
    public interface IOrderStateHistoryService
    {
        /// <summary>
        /// Добавление состояния в историю
        /// </summary>
        /// <param name="newOrderStateHistory"></param>
        /// <returns></returns>
        Task AddOrderStateAsync(OrderStateHistory newOrderStateHistory);

        /// <summary>
        /// Получение истории состояний для выбранного заказа
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Task<IEnumerable<OrderStateHistory>> GetHistoryForOrderByIdAsync(Guid orderId);
    }
}