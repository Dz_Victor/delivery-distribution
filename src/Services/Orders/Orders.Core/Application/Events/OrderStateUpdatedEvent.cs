﻿using System;
using MediatR;

namespace Orders.Core.Application.Events
{
    public class OrderStateUpdatedEvent:INotification
    {
        public Guid OrderId { get; set; }
        
        public int OrderStateId { get; set; }
    }
}