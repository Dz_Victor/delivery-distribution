﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Orders.Core.Abstraction.Services;
using Orders.Core.Application.Events;
using Orders.Core.Domain;

namespace Orders.Core.Application.EventHandlers
{
    public class AddToHistoryWhenOrderStateUpdatedEventHandler
        :INotificationHandler<OrderStateUpdatedEvent>
    {
        private readonly IOrderStateHistoryService _orderStateHistoryService;

        public AddToHistoryWhenOrderStateUpdatedEventHandler(
            IOrderStateHistoryService orderStateHistoryService)
        {
            _orderStateHistoryService = orderStateHistoryService;
        }

        public async Task Handle(OrderStateUpdatedEvent notification, CancellationToken cancellationToken)
        {
            var orderStateHistory = new OrderStateHistory()
            {
                OrderId = notification.OrderId,
                OrderStateId = notification.OrderStateId
            };
            await _orderStateHistoryService.AddOrderStateAsync(orderStateHistory);
        }
    }
}