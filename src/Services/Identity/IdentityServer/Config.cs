﻿// Copyright (c) Duende Software. All rights reserved.
// See LICENSE in the project root for license information.


using System;
using Duende.IdentityServer.Models;
using System.Collections.Generic;
using Duende.IdentityServer;
using IdentityServer.Options;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("orders", "Orders API"),
                new ApiScope("deliveries", "Deliveries API"),
                new ApiScope("couriers", "Couriers API"),
                new ApiScope("customers", "Customers API")
            };

        public static IEnumerable<Client> GetClients(ExternalUrl externalUrl)
        {
            return new List<Client>
            {
                new Client()
                {
                    ClientId = "ordersswaggerui",
                    ClientName = "Orders Swagger UI",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris = {$"{externalUrl.OrdersApiUrl}/swagger/oauth2-redirect.html"},
                    PostLogoutRedirectUris = {$"{externalUrl.OrdersApiUrl}/swagger/"},

                    AllowedScopes =
                    {
                        "orders"
                    }
                },
                new Client()
                {
                    ClientId = "deliveriesswaggerui",
                    ClientName = "Deliveries Swagger UI",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris = {$"{externalUrl.DeliveriesApiUrl}/swagger/oauth2-redirect.html"},
                    PostLogoutRedirectUris = {$"{externalUrl.DeliveriesApiUrl}/swagger/"},

                    AllowedScopes =
                    {
                        "deliveries"
                    }
                },
                new Client()
                {
                    ClientId = "couriersswaggerui",
                    ClientName = "Couriers Swagger UI",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris = {$"{externalUrl.CouriersApiUrl}/swagger/oauth2-redirect.html"},
                    PostLogoutRedirectUris = {$"{externalUrl.CouriersApiUrl}/swagger/"},

                    AllowedScopes =
                    {
                        "couriers"
                    }
                },
                new Client()
                {
                    ClientId = "customersswaggerui",
                    ClientName = "Customers Swagger UI",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris = {$"{externalUrl.CustomersApiUrl}/swagger/oauth2-redirect.html"},
                    PostLogoutRedirectUris = {$"{externalUrl.CustomersApiUrl}/swagger/"},

                    AllowedScopes =
                    {
                        "customers"
                    }
                },
                new Client()
                {
                    ClientId = "ReactSpa",
                    ClientName = "React SPA",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    RedirectUris = {$"{externalUrl.SpaHostUrl}/authentication/login-callback"},
                    RequireConsent = false,
                    PostLogoutRedirectUris = {$"{externalUrl.SpaHostUrl}/authentication/logout-callback"},
                    AllowedCorsOrigins = {$"{externalUrl.SpaHostUrl}"},
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "orders",
                        "deliveries",
                        "customers",
                        "couriers"
                    },
                }
            };
        }
    }
}