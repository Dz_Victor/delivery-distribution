﻿// Copyright (c) Duende Software. All rights reserved.
// See LICENSE in the project root for license information.


using System;
using Deliveries.WebHost.Options;
using Duende.IdentityServer;
using IdentityServer.Certificates;
using IdentityServer.Data;
using IdentityServer.Options;
using IdentityServerHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace IdentityServer
{
    public class Startup
    {
        public IWebHostEnvironment Environment { get; }
        public IConfiguration Configuration { get; }

        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var externalUrlOptions = Configuration.GetSection("ExternalUrl");
            services.Configure<ExternalUrl>(externalUrlOptions);
            var externalUrls = externalUrlOptions.Get<ExternalUrl>();
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(corsBuilder =>
                {
                    corsBuilder
                        .WithOrigins(externalUrls.SpaHostUrl, externalUrls.OrdersApiUrl)
                        .AllowAnyHeader();
                });
            });
            
            services.AddControllersWithViews();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("PostgresConnection"), 
                    sqlOptions =>
                    { 
                        sqlOptions.MigrationsAssembly(typeof(Startup).Assembly.FullName);
                        sqlOptions.EnableRetryOnFailure(15, TimeSpan.FromSeconds(15),
                            null);
                    }));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();



            var builder = services.AddIdentityServer(options =>
                {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseSuccessEvents = true;
                    options.Authentication.CookieLifetime = TimeSpan.FromHours(2);
                    options.IssuerUri = "null";
                    // see https://docs.duendesoftware.com/identityserver/v5/fundamentals/resources/
                    options.EmitStaticAudienceClaim = true;
                })
                .AddInMemoryIdentityResources(Config.IdentityResources)
                .AddInMemoryApiScopes(Config.ApiScopes)
                .AddInMemoryClients(Config.GetClients(externalUrls))
                .AddSigningCredential(Certificate.Get())
                .AddAspNetIdentity<ApplicationUser>();

            services.AddAuthentication()
                // .AddGoogle(options =>
                // {
                //     options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                //     
                //     // register your IdentityServer with Google at https://console.developers.google.com
                //     // enable the Google+ API
                //     // set the redirect URI to https://localhost:5001/signin-google
                //     options.ClientId = Configuration["Google:ClientId"];
                //     options.ClientSecret = Configuration["Google:ClientSecret"];
                // })
                ;
            
            services.AddMassTransitRabbitMq(Configuration);
        }

        public void Configure(IApplicationBuilder app)
        {
            if (Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }

            app.UseStaticFiles();

            app.UseRouting();
            
            app.UseCors();
            
            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
                await next();
            });

            app.UseForwardedHeaders();
            
            app.UseIdentityServer();
            
            app.UseCookiePolicy(new CookiePolicyOptions
            {
                MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.Lax,
            });
            
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }


    public static class ServicesExtensions
    {
        public static IServiceCollection AddMassTransitRabbitMq(
            this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = configuration.GetSection(RabbitMqOptions.SectionName).Get<RabbitMqOptions>();
            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq(
                    (context, cfg) =>
                    {
                        cfg.Host(
                            rabbitSettings.Host,
                            rabbitSettings.Port,
                            rabbitSettings.VirtualHost,
                            conf =>
                            {
                                conf.Username(rabbitSettings.UserName);
                                conf.Password(rabbitSettings.Password);
                            });
                    }
                );
            });

            services.AddMassTransitHostedService();
            return services;
        }
    }
}