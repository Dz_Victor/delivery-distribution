﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Customers.Core.Abstraction.Repositories;
using Customers.Core.Application.Exceptions;
using Customers.Core.Application.Services;
using Customers.Core.Domain;
using Customers.UnitTests.Attributes;
using Customers.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Customers.UnitTests.Core.Application.Services.CustomerServiceTests
{
    public class DeleteCustomerAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task DeleteCustomerAsync_CustomerNotFound_ShouldThrowEntityNotFoundException(
            [Frozen] Mock<IRepository<Customer>> mockRepository,
            [Frozen] CustomerService customerService
        )
        {
            //Arrange
            var customer = CustomerBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(customer.Id)).ReturnsAsync(default(Customer));
                
            //Act
            Func<Task> act = async () => await customerService.DeleteCustomerAsync(customer.Id);

            //Assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }
        
        [Theory, AutoMoqData]
        public async Task DeleteCustomerAsync_SuccessfullyExecuted_ShouldInvokeDeleteMethod(
            [Frozen] Mock<IRepository<Customer>> mockRepository,
            [Frozen] CustomerService customerService
        )
        {
            //Arrange
            var customer = CustomerBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(customer.Id)).ReturnsAsync(customer);
                
            //Act
            await customerService.DeleteCustomerAsync(customer.Id);

            //Assert
            mockRepository.Verify(repo => repo.DeleteAsync(customer), Times.Once);
        }
    }
}