﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Customers.Core.Abstraction.Repositories;
using Customers.Core.Application.Exceptions;
using Customers.Core.Application.Services;
using Customers.Core.Domain;
using Customers.UnitTests.Attributes;
using Customers.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Customers.UnitTests.Core.Application.Services.CustomerServiceTests
{
    public class GetCustomerByIdAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetCustomerByIdAsync_CustomerNotFound_ShouldThrowEntityNotFoundException(
            [Frozen] Mock<IRepository<Customer>> mockRepository,
            [Frozen] CustomerService customerService
        )
        {
            //Arrange
            var customer = CustomerBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(customer.Id)).ReturnsAsync(default(Customer));
                
            //Act
            Func<Task<Customer>> act = async () => await customerService.GetCustomerByIdAsync(customer.Id);

            //Assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Theory, AutoMoqData]
        public async Task GetCustomerByIdAsync_CustomerFound_ShouldReturnFoundCustomer(
            [Frozen] Mock<IRepository<Customer>> mockRepository,
            [Frozen] CustomerService customerService
        )
        {
            //Arrange
            var customer = CustomerBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(customer.Id)).ReturnsAsync(customer);

            //Act
            var result = await customerService.GetCustomerByIdAsync(customer.Id);

            //Assert
            result.Should().BeEquivalentTo(customer, opt => opt.ExcludingMissingMembers());
        }
    }
}