﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Customers.Core.Abstraction.Repositories;
using Customers.Core.Domain;
using Customers.DataAccess.Data;
using Microsoft.EntityFrameworkCore;

namespace Customers.DataAccess.Repositories
{
    public class CustomersRepository:IRepository<Customer>
    {
        private readonly DataContext _dataContext;

        public CustomersRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await _dataContext.Customers.AsNoTracking().ToListAsync();
        }

        public async Task<Customer?> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext.Customers
                .FirstOrDefaultAsync(c => c.Id == id);
            return entity;
        }

        public async Task<Guid> AddAsync(Customer entity)
        {
            _dataContext.Customers.Add(entity);
            await _dataContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(Customer entity)
        {
            _dataContext.Customers.Update(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Customer entity)
        {
            _dataContext.Customers.Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Customer>> GetOnConditionAsync(Expression<Func<Customer, bool>> predicate)
        {
            return await _dataContext.Customers
                .AsNoTracking()
                .Where(predicate)
                .ToListAsync();
        }
    }
}