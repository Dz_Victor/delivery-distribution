﻿using AutoMapper;
using Customers.Core.Domain;
using Customers.WebHost.Dto.Customer;

namespace Customers.WebHost.Mappers
{
    public class AutoMapperCustomerProfiles : Profile
    {
        public AutoMapperCustomerProfiles()
        {
            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<CustomerShortResponse, Customer>();

            CreateMap<Customer, CreateOrEditCustomerRequest>();
            CreateMap<CreateOrEditCustomerRequest, Customer>();

            CreateMap<Customer, CustomerDetailedResponse>();
            CreateMap<CustomerDetailedResponse, Customer>();                       
        }
    }
}
