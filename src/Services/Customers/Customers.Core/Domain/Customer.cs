﻿using System.Collections.Generic;

namespace Customers.Core.Domain
{
    public class Customer : BaseEntity
    {
        /// <summary>
        /// Фамилия, имя и отчество.  
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string? PhoneNumber { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string? Email { get; set; }
        
    }

}