﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Customers.Core.Domain;

namespace Customers.Core.Abstraction.Services
{
    /// <summary>
    /// Сервис для работы с клиентами
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        Task<IEnumerable<Customer>> GetCustomersAsync();

        /// <summary>
        /// Получить данные клиента по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Customer> GetCustomerByIdAsync(Guid id);

        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <param name="newCustomer"></param>
        /// <returns></returns>
        Task<Guid> AddCustomerAsync(Customer newCustomer);

        /// <summary>
        /// Изменить данные клиента по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedCustomer"></param>
        /// <returns></returns>
        Task EditCustomerAsync(Guid id, Customer updatedCustomer);

        /// <summary>
        /// Удалить клиента по идентификатору вместе с его заказами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteCustomerAsync(Guid id);

    }
}
